﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [HideInInspector] public float minX, maxX, minY, maxY;

    public static PlayerController Instance;
    public static event Action<CircleController> circleChangeEvent;

    public bool isSpin;
    [HideInInspector] public Transform centerPoint;
    public float radius = 1f;
    public float speedSpin = 1.0f;
    public float speed = 1.0f;
    float angle = 0f;
    GhostSprites ghostSprite;
    public GameObject fxExplosion;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        circleChangeEvent = null;
        minX = Camera.main.ViewportToWorldPoint(Vector2.zero).x;
        minY = Camera.main.ViewportToWorldPoint(Vector2.zero).y;
        maxX = Camera.main.ViewportToWorldPoint(Vector2.right).x;
        maxY = Camera.main.ViewportToWorldPoint(Vector2.up).y;
    }
    private void Start()
    {
        
        ghostSprite = GetComponent<GhostSprites>();

        if (ghostSprite)
        {
            ghostSprite.allowGhost = false;
        }
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isSpin = false;
            ghostSprite.allowGhost = !isSpin;
            Shoot();
        }
        if (isSpin)
            Spin();
        if (transform.position.x < minX || transform.position.x > maxX || transform.position.y < minY || transform.position.y > maxY)
        {
            onDie();
        }

    }
    public void SetAngle(Vector3 _pos)
    {
        Vector3 pos = FindIntersectionPoint(_pos);

        float deltaX = pos.x - centerPoint.position.x;
        float deltaY = pos.y - centerPoint.position.y;

        angle = Mathf.Atan2(deltaY, deltaX);
    }
    Vector3 FindIntersectionPoint(Vector3 pos)
    {
        Vector3 vectorOA = pos - centerPoint.position;
        vectorOA.Normalize();

        // Tính tọa độ của điểm A trên đường tròn
        Vector3 pointA = centerPoint.position + radius * vectorOA;
        return pointA;
    }
    public void Spin()
    {
        float x = centerPoint.position.x + Mathf.Cos(angle) * radius;
        float y = centerPoint.position.y + Mathf.Sin(angle) * radius;

        transform.position = new Vector2(x, y);
        angle += speedSpin * Time.deltaTime;
    }
    public void ChangeCircle(CircleController circle)
    {
        if (circleChangeEvent != null)
        {
            circleChangeEvent(circle);
        }
        
        centerPoint = circle.transform;
        SetAngle(transform.position);
        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            speedSpin = -speedSpin;
        }
        isSpin = true;
        ghostSprite.allowGhost = !isSpin;
        CircleManager.Instance.SetCircleBlack(circle);
        CircleManager.Instance.SetRandomCircleWhite();
        CircleManager.Instance.SetCircleDefault();
    }
    public void Shoot()
    {
        
        Vector2 direction = transform.position - centerPoint.position;  
        gameObject.GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Circle"))
        {
            if (collision == null)
            {
                return;
            }
            CircleController circle = collision.gameObject.GetComponent<CircleController>();
            if (circle != null && circle.state == State.white)
            {
                ChangeCircle(circle);
            }
        }
    }
    void onDie()
    {
        if (fxExplosion)
        {
            Instantiate(fxExplosion, transform.position, Quaternion.identity);
        }
        MenuManager.Instance.GameOver();
        Destroy(gameObject);
    }
}
