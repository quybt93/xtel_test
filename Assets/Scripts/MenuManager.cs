using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance;
    public GameObject uI, gameOver, loadingUI;
    public Text txtScores;
    public Text txtScoresEnd;
    public int Point { get; set; }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        
    }
    private void Update()
    {
    }
    void Start()
    {
        uI.SetActive(true);
        gameOver.SetActive(false);
        loadingUI.SetActive(false);
        txtScores.text = Point.ToString();
        PlayerController.circleChangeEvent += AddPoint;
    }

    public void Play()
    {
        uI.SetActive(true);
    }

    public void GameOver()
    {
        StopAllCoroutines();
        Invoke("GameOverCo", 1);
    }

    void GameOverCo()
    {
        uI.SetActive(false);
        txtScoresEnd.text = Point.ToString();
        gameOver.SetActive(true);
    }

    public void Restart()
    {
        uI.SetActive(false);
        loadingUI.SetActive(true );
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //public void Home()
    //{
    //    LoadingUI.SetActive(true);
    //    SceneManager.LoadSceneAsync("MainMenu");
    //}
    public void AddPoint(CircleController circle)
    {
        Point += 1;
        txtScores.text = Point.ToString();
    }
}
