using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    grey, white, black
}
public class CircleController : MonoBehaviour
{
    public State state = State.grey;
    private void Start()
    {
        float radius = PlayerController.Instance.radius;
        float minX = PlayerController.Instance.minX + radius;
        float maxX = PlayerController.Instance.maxX - radius;
        float minY = PlayerController.Instance.minY + radius;
        float maxY = PlayerController.Instance.maxY - radius;
        
        if (transform.position.x < minX)
        {
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
        if (transform.position.x > maxX)
        {
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }
        if (transform.position.y < minY)
        {
            transform.position = new Vector3(transform.position.x, minY, transform.position.z);
        }
        if (transform.position.y > maxY)
        {
            transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        }

    }
    public void ChangeState(State _state)
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.LogError("SpriteRenderer component not found on " + gameObject.name);
        }
        state = _state;
        switch (state)
        {
            case State.white:
                spriteRenderer.color = Color.white;
                break;
            case State.black:
                spriteRenderer.color = Color.black;
                break;
            case State.grey:
                spriteRenderer.color = Color.grey;  
                break;
            default:
                break;
        }
    }
}
