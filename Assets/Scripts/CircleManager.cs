using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleManager : MonoBehaviour
{
    public static CircleManager Instance;

    CircleController[] circles;
    int circleBlack;
    int circleWhite;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    void Start()
    {
        
        circles = FindObjectsOfType<CircleController>();
        int temp = Random.Range(0, circles.Length);
        SetCircleBlack(circles[temp]);
        PlayerController.Instance.centerPoint = circles[temp].transform;
        SetRandomCircleWhite();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeCircle()
    {

    }
    public void SetRandomCircleWhite()
    {
        if (circles.Length > 1)
        {
            do
            {
                circleWhite = Random.Range(0, circles.Length);
            } while (circleWhite == circleBlack);
            circles[circleWhite].ChangeState(State.white);
            SetCircleDefault();
        }
        else
        {
            Debug.LogError("Insufficient CircleController objects to set CircleWhite.");
        }
    }
    public void SetCircleDefault()
    {
        for (int i = 0; i < circles.Length; i++)
        {
            if (i != circleWhite && i != circleBlack)
            {
                circles[i].ChangeState(State.grey);
            }
        }
    }
    public void SetCircleBlack(CircleController circle)
    {
        circle.ChangeState(State.black);
        
        for (int i = 0; i < circles.Length; i++)
        {
            if (circle == circles[i])
            {
                circleBlack = i;
                break;
            }
        }
    }
}
